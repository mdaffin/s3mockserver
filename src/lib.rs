use serde::{Deserialize, Serialize};
use std::{
    str::FromStr,
    sync::{Arc, Mutex},
};

#[cfg(feature = "aws-sdk")]
use aws_sdk_s3::{Config, Region};
#[cfg(feature = "aws-sdk")]
use aws_smithy_http::endpoint::Endpoint;
#[cfg(feature = "aws-sdk")]
use aws_types::Credentials;
use http::Uri;
use response_types::*;
use std::collections::HashMap;
use tide::{listener::Listener, Body};
use tide::{Request, Response};
use tokio::select;
use tokio::sync::oneshot;

pub struct S3TestServer {
    addr: String,
    _shutdown: oneshot::Sender<()>,
}

type Buckets = Arc<Mutex<HashMap<String, HashMap<String, Vec<u8>>>>>;

async fn list_buckets(req: Request<Buckets>) -> tide::Result<impl Into<Response>> {
    let buckets = req
        .state()
        .lock()
        .unwrap()
        .keys()
        .map(|bucket| Bucket {
            name: bucket.into(),
            creation_date: "2021-06-20T15:50:21.715Z".into(),
        })
        .collect::<Vec<_>>()
        .into();
    let mut body = r#"<?xml version="1.0" encoding="UTF-8"?>"#.to_string();
    body.push('\n');
    body.push_str(
        &quick_xml::se::to_string(&ListAllMyBucketsResult {
            xmlns: "http://s3.amazonaws.com/doc/2006-03-01/".to_string(),
            owner: Owner {
                id: "0".into(),
                display_name: "s3mockserver".into(),
            },
            buckets,
        })
        .unwrap(),
    );
    Ok(body)
}

async fn create_bucket(req: Request<Buckets>) -> tide::Result<impl Into<Response>> {
    let bucket_name = req.param("bucket")?.into();
    req.state()
        .lock()
        .unwrap()
        .insert(bucket_name, HashMap::new());
    Ok(String::new())
}

async fn delete_bucket(req: Request<Buckets>) -> tide::Result<impl Into<Response>> {
    let bucket_name = req.param("bucket")?;
    req.state().lock().unwrap().remove(bucket_name);
    Ok(String::from('\r'))
}

async fn list_object(req: Request<Buckets>) -> tide::Result<impl Into<Response>> {
    let bucket_name = req.param("bucket")?;
    let objects = req
        .state()
        .lock()
        .unwrap()
        .get(bucket_name)
        .unwrap()
        .keys()
        .map(|key| Content { key: key.into() })
        .collect::<Vec<_>>();
    let mut body = r#"<?xml version="1.0" encoding="UTF-8"?>"#.to_string();
    body.push('\n');
    body.push_str(
        &quick_xml::se::to_string(&ListBucketResult {
            xmlns: "http://s3.amazonaws.com/doc/2006-03-01/".to_string(),
            name: "foo.txt".into(),
            prefix: Prefix(None),
            contents: objects,
        })
        .unwrap(),
    );
    Ok(body)
}

async fn put_object(mut req: Request<Buckets>) -> tide::Result<impl Into<Response>> {
    let body = req.body_bytes().await?;
    assert_eq!(req.query::<Params>()?.x_id, "PutObject");
    let bucket_name = req.param("bucket")?;
    let key = percent_encoding::percent_decode(req.param("key")?.as_bytes())
        .decode_utf8()
        .unwrap();

    req.state()
        .lock()
        .unwrap()
        .get_mut(bucket_name)
        .unwrap()
        .insert(key.to_string(), body);
    Ok("")
}

async fn delete_object(req: Request<Buckets>) -> tide::Result<impl Into<Response>> {
    let bucket_name = req.param("bucket")?;
    let key = percent_encoding::percent_decode(req.param("key")?.as_bytes())
        .decode_utf8()
        .unwrap();

    req.state()
        .lock()
        .unwrap()
        .get_mut(bucket_name)
        .unwrap()
        .remove(&key.to_string());

    Ok("")
}

async fn get_object(req: Request<Buckets>) -> tide::Result<impl Into<Response>> {
    let bucket_name = req.param("bucket")?;
    let key = percent_encoding::percent_decode(req.param("key")?.as_bytes())
        .decode_utf8()
        .unwrap();

    Ok(
        match req
            .state()
            .lock()
            .unwrap()
            .get(bucket_name)
            .unwrap()
            .get(&key.to_string())
        {
            Some(body) => {
                let mut response = Response::new(tide::http::StatusCode::Ok);
                response.set_body(Body::from_bytes(body.clone()));
                response
            }
            None => {
                let mut response = Response::new(tide::http::StatusCode::NotFound);
                response.set_body(Body::from_string("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Error><Code>NoSuchKey</Code><Message>The specified key does not exist.</Message><Key>does not exist</Key><BucketName>foo</BucketName><Resource>/foo/does not exist</Resource><RequestId>1696EC5699D03CB7</RequestId><HostId>d70cf048-693f-4087-844f-3ce77f550f36</HostId></Error>".to_string()));
                response
            }
        },
    )
}

impl S3TestServer {
    pub async fn new() -> Self {
        let mut app = tide::with_state(Buckets::new(Mutex::new(HashMap::<
            String,
            HashMap<String, Vec<u8>>,
        >::new())));

        app.at("/").get(list_buckets);
        app.at("/:bucket")
            .put(create_bucket)
            .get(list_object)
            .delete(delete_bucket);
        app.at("/:bucket/*key")
            .put(put_object)
            .get(get_object)
            .delete(delete_object);

        let mut listener = app.bind("127.0.0.1:0").await.unwrap();
        let addr = listener.info().get(0).unwrap().connection().to_owned();

        let (shutdown, rx) = oneshot::channel();
        tokio::spawn(async move {
            select! {
                _ = listener.accept() => (),
                _ = rx => (),
            }
        });

        S3TestServer {
            addr,
            _shutdown: shutdown,
        }
    }

    pub fn endpoint_url(&self) -> Uri {
        Uri::from_str(&self.addr).unwrap()
        //Uri::from_str("http://localhost:9000").unwrap()
    }

    pub fn access_key_id(&self) -> &str {
        "admin"
    }

    pub fn secret_access_key(&self) -> &str {
        "password"
    }

    pub fn region(&self) -> &str {
        "us-east-1"
    }

    #[cfg(feature = "aws-sdk")]
    pub fn config(&self) -> Config {
        Config::builder()
            .endpoint_resolver(Endpoint::immutable(self.endpoint_url()))
            .credentials_provider(Credentials::from_keys(
                self.access_key_id(),
                self.secret_access_key(),
                None,
            ))
            .region(Region::new(self.region().to_string()))
            .build()
    }
}

#[derive(PartialEq, Serialize, Deserialize, Debug)]
struct Params {
    #[serde(rename = "x-id")]
    x_id: String,
}

mod response_types {
    use serde::{Deserialize, Serialize};

    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    #[serde(rename_all = "PascalCase")]
    pub struct ListAllMyBucketsResult {
        #[serde(rename = "xmlns")]
        pub xmlns: String,
        pub owner: Owner,
        pub buckets: Buckets,
    }

    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    #[serde(rename_all = "PascalCase")]
    pub struct ListBucketResult {
        #[serde(rename = "xmlns")]
        pub xmlns: String,
        pub name: Name,
        pub prefix: Prefix,
        #[serde(rename = "Contents")]
        pub contents: Vec<Content>,
    }

    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    #[serde(rename_all = "PascalCase")]
    pub struct Content {
        pub key: Key,
    }

    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    pub struct Buckets {
        #[serde(rename = "Bucket", default)]
        pub buckets: Vec<Bucket>,
    }

    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    #[serde(rename_all = "PascalCase")]
    pub struct Owner {
        #[serde(rename = "ID")]
        pub id: ID,
        pub display_name: DisplayName,
    }

    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    #[serde(rename_all = "PascalCase")]
    pub struct Bucket {
        pub name: Name,
        pub creation_date: CreationDate,
    }

    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    pub struct Key(pub String);
    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    pub struct Prefix(pub Option<String>);
    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    pub struct ID(pub String);
    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    pub struct DisplayName(pub String);
    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    pub struct Name(pub String);
    #[derive(PartialEq, Serialize, Deserialize, Debug)]
    pub struct CreationDate(pub String);

    impl<T: Into<Vec<Bucket>>> From<T> for Buckets {
        fn from(src: T) -> Self {
            Self {
                buckets: src.into(),
            }
        }
    }
    impl<T: Into<String>> From<T> for Key {
        fn from(src: T) -> Self {
            Self(src.into())
        }
    }
    impl<T: Into<String>> From<T> for ID {
        fn from(src: T) -> Self {
            Self(src.into())
        }
    }
    impl<T: Into<String>> From<T> for DisplayName {
        fn from(src: T) -> Self {
            Self(src.into())
        }
    }
    impl<T: Into<String>> From<T> for Name {
        fn from(src: T) -> Self {
            Self(src.into())
        }
    }
    impl<T: Into<String>> From<T> for CreationDate {
        fn from(src: T) -> Self {
            Self(src.into())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use bytes::Bytes;
    use proptest::prelude::*;
    use proptest::string::bytes_regex;

    use aws_sdk_s3::{ByteStream, Client, SdkError};
    use std::collections::HashSet;
    use std::iter::FromIterator;

    #[tokio::test]
    async fn listing_buckets_should_return_empty_list_by_default() {
        let ts = S3TestServer::new().await;
        let client = Client::from_conf(ts.config());
        let buckets = client.list_buckets().send().await.unwrap();
        assert_eq!(buckets.buckets, Some(Vec::new()));
    }

    #[tokio::test]
    async fn deleting_the_only_bucket_should_list_no_buckets() {
        let bucket = "foo";
        let ts = S3TestServer::new().await;
        let client = Client::from_conf(ts.config());

        client
            .create_bucket()
            .bucket(bucket)
            .send()
            .await
            .expect(&format!("Failed creating bucket: {}", bucket));
        client
            .delete_bucket()
            .bucket(bucket)
            .send()
            .await
            .expect(&format!("Failed deleting bucket: {}", bucket));

        let buckets = client.list_buckets().send().await.unwrap();
        assert_eq!(buckets.buckets, Some(Vec::new()));
    }

    #[tokio::test]
    async fn deleting_an_empty_bucket() {
        let bucket = "foo";
        let ts = S3TestServer::new().await;
        let client = Client::from_conf(ts.config());

        client
            .create_bucket()
            .bucket(bucket)
            .send()
            .await
            .expect(&format!("Failed creating bucket: {}", bucket));
        client
            .create_bucket()
            .bucket("bar")
            .send()
            .await
            .expect(&format!("Failed creating bucket: {}", bucket));
        client
            .delete_bucket()
            .bucket(bucket)
            .send()
            .await
            .expect(&format!("Failed deleting bucket: {}", bucket));

        let buckets = client.list_buckets().send().await.unwrap();
        assert_eq!(buckets.buckets.unwrap().len(), 1);
    }

    #[tokio::test]
    async fn display_name_should_be_s3mockserver() {
        let ts = S3TestServer::new().await;
        let client = Client::from_conf(ts.config());
        let buckets = client.list_buckets().send().await.unwrap();
        assert_eq!(
            &buckets.owner.unwrap().display_name.unwrap(),
            "s3mockserver"
        );
    }

    #[tokio::test]
    async fn listing_objects_should_return_empty_list_for_a_new_bucket() {
        let bucket = "foo";

        let ts = S3TestServer::new().await;
        let client = Client::from_conf(ts.config());
        client.create_bucket().bucket(bucket).send().await.unwrap();
        let objects = client.list_objects().bucket(bucket).send().await.unwrap();
        assert_eq!(objects.contents, None);
    }

    #[tokio::test]
    async fn get_object_returns_error_if_object_does_not_exist() {
        let bucket = "foo";
        let key = "does not exist";

        let ts = S3TestServer::new().await;
        let client = Client::from_conf(ts.config());
        client.create_bucket().bucket(bucket).send().await.unwrap();

        let err = client
            .get_object()
            .bucket(bucket)
            .key(key)
            .send()
            .await
            .expect_err("no error form server");

        match err {
            SdkError::ServiceError { err, .. } => match err.kind {
                aws_sdk_s3::error::GetObjectErrorKind::NoSuchKey(
                    aws_sdk_s3::error::NoSuchKey { .. },
                ) => {}
                _ => panic!("error is not 'no such key': {}", err),
            },
            err => panic!("error is not 'no such key': {}", err),
        }
    }

    proptest! {
        #[test]
        fn listing_objects_should_return_all_added_objects_for_a_bucket(
            // both space and tab _should_ work but there is a bug in aws-sdk-rust that strips
            // whitespace on object keys so we cannot validate the results. https://github.com/awslabs/aws-sdk-rust/issues/153
            objects in prop::collection::vec(r#"[^(\s+)\.(\.\.)([\pC\\]+)<>`]"#, 1..=5).prop_filter("duplicate value in list", |list| {
                HashSet::<&str>::from_iter(list.iter().map(String::as_str)).len() == list.len()
            }),
        ) {
            tokio::runtime::Runtime::new().unwrap().block_on(async {
                let bucket = "foo";

                let ts = S3TestServer::new().await;
                let client = Client::from_conf(ts.config());
                client.create_bucket().bucket(bucket).send().await.unwrap();

                for object in &objects {
                    client
                        .put_object()
                        .bucket(bucket)
                        .key(object)
                        .send()
                        .await
                        .expect("uploading object failed");
                }

                let returned_objects = client
                    .list_objects()
                    .bucket(bucket)
                    .send()
                    .await
                    .expect("listing obejcts failed")
                    .contents
                    .expect("no contents");
                let returned_objects = returned_objects
                    .iter()
                    .map(|object| object.key.as_ref().expect("no key"))
                    .collect::<Vec<_>>();

                assert_eq!(objects.len(), returned_objects.len());
                for object in objects {
                    assert!(returned_objects.contains(&&object));
                }
            })
        }
    }

    proptest! {
        #[test]
        fn listing_buckets_should_list_created_buckets(
            buckets in prop::collection::vec("[a-zA-Z0-9][-\\.a-zA-Z0-9]{1,61}[a-zA-Z0-9]", 1..=10),
        ) {
            tokio::runtime::Runtime::new().unwrap().block_on(async {
                let ts = S3TestServer::new().await;
                let client = Client::from_conf(ts.config());

                for bucket in &buckets {
                    client.create_bucket().bucket(bucket).send().await.unwrap();
                }

                let return_buckets = client.list_buckets().send().await.unwrap().buckets.unwrap();
                let return_buckets = return_buckets.iter()
                        .map(|bucket| bucket.name.as_ref().unwrap())
                        .collect::<Vec<_>>();

                assert_eq!(buckets.len(), return_buckets.len());
                for bucket in buckets {
                    assert!(return_buckets.contains(&&bucket));
                }
            })
        }
    }

    proptest! {
        #[test]
        fn getting_an_object_returns_the_same_contents_that_it_was_put_with(
                contents in bytes_regex(".*").unwrap(),
            ) {
            tokio::runtime::Runtime::new().unwrap().block_on(async {
                let bucket = "foo";
                let key = "foo.txt";

                let ts = S3TestServer::new().await;
                let client = Client::from_conf(ts.config());
                client.create_bucket().bucket(bucket).send().await.unwrap();

                client
                    .put_object()
                    .bucket(bucket)
                    .key(key)
                    .body(ByteStream::from(contents.to_vec()))
                    .send()
                    .await
                    .expect("uploading object failed");

                let object = client
                    .get_object()
                    .bucket(bucket)
                    .key(key)
                    .send()
                    .await
                    .expect("getting object failed");

                let body = object
                    .body
                    .collect()
                    .await
                    .expect("failed to get object body")
                    .into_bytes();

                assert_eq!(Bytes::from(contents.to_vec()), body);
            })
        }
    }

    #[test]
    fn deleteing_and_object_removes_it_from_the_store() {
        tokio::runtime::Runtime::new().unwrap().block_on(async {
            let bucket = "foo";
            let key = "foo.txt";

            let ts = S3TestServer::new().await;
            let client = Client::from_conf(ts.config());
            client.create_bucket().bucket(bucket).send().await.unwrap();

            client
                .put_object()
                .bucket(bucket)
                .key(key)
                .send()
                .await
                .expect("uploading object failed");

            client
                .delete_object()
                .bucket(bucket)
                .key(key)
                .send()
                .await
                .expect("deleting object failed");

            let objects = client.list_objects().bucket(bucket).send().await.unwrap();

            assert!(objects.contents.is_none());
        })
    }
}
